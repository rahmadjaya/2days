console.log('four');

//FIX HEADER
$(function(){
  $(window).scroll(function(){
    var top = $(window).scrollTop();
    if(top >= 40){
      $('.page-four').addClass('sticky-header');
    }else{
      $('.page-four').removeClass('sticky-header');
    }
  });
});

//nav active
$('nav ul li a').click(function(){
  $('ul li a').removeClass('active');
  $(this).addClass('active');
})
//tabs active
$('.caption ul li').click(function(){
	$('ul li').removeClass('active');
	$(this).addClass('active');
})

//show tabs
$(document).ready(function(){
  $('.tab').hide();
  $('#tab_1').show();
  $('.tabs li').click(function(){
    var id = $(this).attr('id');
    $('.tab').hide();
    $('#tab_'+id).toggle();
  });
});