console.log('five');

//nav
$('#brand').click(function() {
	$('nav').toggle('slow');
})

// //scroll
$(window).scroll(function(){
  var y = $(window).scrollTop()
  if (y <= 300){
    $('a').css('color','white');

    $('#header').css('background-color','#da1c5c');
  } else {
    $('#header').css('background-color','#27094d');
    $('#header').css('color','white');
    $('a').css('color','#da1c5c');
  }
});

//show
$(document).ready(function(){
  $('.show').hide();
  $('.image .icon').click(function(){
    var id = $(this).attr('id');
    $('.show').hide();
    $('#show_'+id).toggle('slow');
  });
});
$('.hide').click(function(){
	$('.show').hide('slow');
})